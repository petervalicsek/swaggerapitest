import docker
import os
import time

client = docker.from_env()
print(client.images.list())

app = client.images.build(path="/home/valicsek/work/swagger/app/", rm=True)

swagger_app = client.images.get("swagger_app:latest")
swagger_db = client.images.get("postgres:10.5-alpine")
print(swagger_app.id)
print(swagger_db.id)

print("Startig containers...")
client.containers.run(swagger_db, detach = True, volumes={'/home/valicsek/work/swagger/db/init.sql':{'bind': '/docker-entrypoint-initdb.d/init.sql'}}, name = "db", remove = True)
print("DB started")
client.containers.run(swagger_app, detach = True, volumes={'/home/valicsek/work/swagger/app/':{'bind': '/swagger/'}}, name = "app", ports = {"5000/tcp":5000}, links = {"db": "db"}, remove = True)
print("App started")
