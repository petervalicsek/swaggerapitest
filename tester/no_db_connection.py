""" no db connection test """
import unittest
import requests
import urllib3

class TestMethods(unittest.TestCase):


    """ Test cases """
    def test_no_con(self):
        try:
            resp = requests.get("http://app:5000/v1.0/users/")
        except Exception as e:
            # self.assertEqual(requests.exceptions.ConnectionError, type(e))
            self.assertRegex(str(e), ".*\[Errno -2\].*")
if __name__ == '__main__':
    unittest.main()
