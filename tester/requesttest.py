""" Request tests """
import unittest
import requests
import io
import time

class TestMethods(unittest.TestCase):
    """ Test cases """

    tries = 10
    while tries >= 0:
        try:
            resp = requests.get("http://app:5000/v1.0/users/")
            break
        except Exception as e:
            time.sleep(2)
            tries -= 1

    last_id = 0

    def test1_post(self):
        """ post test """
        # headers = {"content-type": "application/json"}-t be kell állítani, ha data-ként küldöm
        resp = requests.post("http://app:5000/v1.0/user/", json={"name": "Peti"})
        self.assertRegex(resp.text, "[0-9]+", msg="Not a number")
        TestMethods.last_id = resp.json()

    def test2_put(self):
        """ put test """
        data = {"id": self.last_id, "name": "Gyuri"}
        resp = requests.put("http://app:5000/v1.0/user/", json=data)
        self.assertEqual(resp.json(), {"name": "Gyuri", "id": self.last_id})

    def test3_get_all(self):
        """ get all test """
        resp = requests.get("http://app:5000/v1.0/users/")
        self.assertRegex(resp.text, "\[?({'id': \d*, 'name': '\w*'},?)*\]?", msg="Not equal")

    def test4_get(self):
        """ get test """
        resp = requests.get("http://app:5000/v1.0/user/?id="+str(self.last_id))
        self.assertEqual(resp.json(), {"name": "Gyuri"})

    def test5_del(self):
        """ delete test """
        resp = requests.delete("http://app:5000/v1.0/user/", json={"id": self.last_id})
        self.assertEqual(resp.json(), "Delete of id: "+str(self.last_id)+" completed")

    def test6_del_not_found(self):
        """ delete not found test """
        resp = requests.delete("http://app:5000/v1.0/user/", json={"id": 0})
        self.assertEqual(resp.json(), "User not found")


    def test7_put_not_found(self):
        """ put not found test """
        resp = requests.put("http://app:5000/v1.0/user/", json={"id": 0, "name":"Gyuri"})
        self.assertEqual(resp.json(), "User not found")

    def test8_get_not_found(self):
        """ get not found test """
        resp = requests.get("http://app:5000/v1.0/user/?id=0")
        self.assertEqual(resp.json(), "User not found")

    def test9_download(self):
        """ download test """
        resp = requests.get("http://app:5000/v1.0/download", json={"filename":"app.py"})
        self.assertEqual(resp.status_code, 200)

    def test10_download_not_found(self):
        """ download not found test """
        resp = requests.get("http://app:5000/v1.0/download", json={"filename":"asdasdasd.asd"})
        self.assertEqual(resp.status_code, 404)
    #
    # def test11_upload_test(self):
    #     """ upload test """
    #     url = "http://app:5000/v1.0/upload"
    #     files = {"file": ("valami.txt")}
    #     resp = requests.post(url, files=files)
    #     print(str(resp))

    def test12_wrong_url(self):
        """ wrong URL test """
        try:
            resp = requests.get("http://app:5010/v1.0/users")
        except Exception as e:
            # print(str(e))
            self.assertRegex(str(e), ".*\[Errno 111\].*")

if __name__ == '__main__':
    unittest.main()
