import swagger_client
import time
import ast

api_instance = swagger_client.DefaultApi()

def print_out():
    resp = api_instance.app_get_users()
    # str --> dictionary
    resp = ast.literal_eval(resp)
    for user in resp:
        print("id: " + str(user['id']) + ", name: " +user['name'])

def add_new_user():
    kwargs = {"user":{"name":"Peti"}}
    api_instance.app_add_user(**kwargs)

secs = 0


while secs <= 6:
    print(secs)
    if secs % 3 == 0:
        add_new_user()
    if secs % 1 == 0:
        print_out()

    time.sleep(1)
    secs += 1
