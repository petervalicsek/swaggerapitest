#!/bin/sh
# cd local/out/python
python3 local/out/python/setup.py install
echo success
python3 swagger/sdkrun.py

# set -exo pipefail
#
# cd "$(dirname ${BASH_SOURCE})"
#
# maven_cache_repo="${HOME}/.m2/repository"
#
# mkdir -p "${maven_cache_repo}"
#
# docker run --rm -it \
#         -w /gen \
#         -e GEN_DIR=/gen \
#         -e MAVEN_CONFIG=/var/maven/.m2 \
#         -u "$(id -u):$(id -g)" \
#         -v "${PWD}:/gen" \
#         -v "${maven_cache_repo}:/var/maven/.m2/repository" \
#         --entrypoint /gen/docker-entrypoint.sh \
# maven:3-jdk-7 "$@"




# generate -l python -i /swagger/myswaggerapi.yaml -o /local/out/python
