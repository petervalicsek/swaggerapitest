# swagger_client.DefaultApi

All URIs are relative to *http://app:5000/v1.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**app_add_user**](DefaultApi.md#app_add_user) | **POST** /user/ | add user
[**app_delete_user**](DefaultApi.md#app_delete_user) | **DELETE** /user/ | delete user
[**app_download_file**](DefaultApi.md#app_download_file) | **GET** /download | downloads a file
[**app_edit_user**](DefaultApi.md#app_edit_user) | **PUT** /user/ | updates an user
[**app_get_user**](DefaultApi.md#app_get_user) | **GET** /user/ | get user
[**app_get_users**](DefaultApi.md#app_get_users) | **GET** /users/ | Get all users
[**app_upload_file**](DefaultApi.md#app_upload_file) | **POST** /upload | uploads a file


# **app_add_user**
> app_add_user(user=user)

add user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
user = swagger_client.User() # User |  (optional)

try:
    # add user
    api_instance.app_add_user(user=user)
except ApiException as e:
    print("Exception when calling DefaultApi->app_add_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **app_delete_user**
> str app_delete_user(user=user)

delete user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
user = swagger_client.User() # User |  (optional)

try:
    # delete user
    api_response = api_instance.app_delete_user(user=user)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->app_delete_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md)|  | [optional] 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **app_download_file**
> file app_download_file(filename=filename)

downloads a file

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
filename = '/path/to/file' # File |  (optional)

try:
    # downloads a file
    api_response = api_instance.app_download_file(filename=filename)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->app_download_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | [**File**](File.md)|  | [optional] 

### Return type

[**file**](file.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **app_edit_user**
> app_edit_user(user=user)

updates an user

update an user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
user = swagger_client.User() # User |  (optional)

try:
    # updates an user
    api_instance.app_edit_user(user=user)
except ApiException as e:
    print("Exception when calling DefaultApi->app_edit_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **app_get_user**
> app_get_user(id=id)

get user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
id = 56 # int |  (optional)

try:
    # get user
    api_instance.app_get_user(id=id)
except ApiException as e:
    print("Exception when calling DefaultApi->app_get_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **app_get_users**
> str app_get_users()

Get all users

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get all users
    api_response = api_instance.app_get_users()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->app_get_users: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **app_upload_file**
> app_upload_file(upfile=upfile)

uploads a file

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
upfile = '/path/to/file.txt' # file | The file to upload (optional)

try:
    # uploads a file
    api_instance.app_upload_file(upfile=upfile)
except ApiException as e:
    print("Exception when calling DefaultApi->app_upload_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **upfile** | **file**| The file to upload | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

