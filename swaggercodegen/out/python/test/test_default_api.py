# coding: utf-8

"""
    {{title}}

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.default_api import DefaultApi  # noqa: E501
from swagger_client.rest import ApiException


class TestDefaultApi(unittest.TestCase):
    """DefaultApi unit test stubs"""

    def setUp(self):
        self.api = swagger_client.api.default_api.DefaultApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_app_add_user(self):
        """Test case for app_add_user

        add user  # noqa: E501
        """
        pass

    def test_app_delete_user(self):
        """Test case for app_delete_user

        delete user  # noqa: E501
        """
        pass

    def test_app_download_file(self):
        """Test case for app_download_file

        downloads a file  # noqa: E501
        """
        pass

    def test_app_edit_user(self):
        """Test case for app_edit_user

        updates an user  # noqa: E501
        """
        pass

    def test_app_get_user(self):
        """Test case for app_get_user

        get user  # noqa: E501
        """
        pass

    def test_app_get_users(self):
        """Test case for app_get_users

        Get all users  # noqa: E501
        """
        pass

    def test_app_upload_file(self):
        """Test case for app_upload_file

        uploads a file  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
