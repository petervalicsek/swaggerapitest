""" Simple tests """
import unittest
from app import add_user, get_user, get_users, edit_user, delete_user, get_last_id

class TestMethods(unittest.TestCase):
    """ Test cases """
    def test1_post(self):
        """ post test """
        self.assertRegex(str(add_user({"name": "Peti"})), "[0-9]+", msg="Not a number")

    def test2_put(self):
        """ put test """
        last_id = get_last_id()
        self.assertEqual(edit_user({"id":last_id, "name":"Feri"}), {"name":"Feri", "id":last_id})

    def test3_get_all(self):
        """ get all test """
        last_id = get_last_id()
        self.assertRegex(str(get_users()), "\[?({'id': \d*, 'name': '\w*'},?)*\]?", msg="Not equal")

    def test4_get(self):
        """ get test """
        last_id = get_last_id()
        self.assertEqual(get_user(last_id), {"name": "Feri"})

    def test5_del(self):
        """ delete test """
        last_id = get_last_id()
        self.assertEqual(delete_user({"id": last_id}), "Delete of id: "+str(last_id)+" completed")

    def test6_del_not_found(self):
        """ delete not found test """
        self.assertEqual(delete_user({"id": 0}), ("User not found", 404))

    def test7_put_not_found(self):
        """ put not found test """
        self.assertEqual(edit_user({"id": 0, "name":"Gyuri"}), ("User not found", 404))

    def test8_get_not_found(self):
        """ get not found test """
        self.assertEqual(get_user({"id": 0}), ("User not found", 404))


if __name__ == '__main__':
    unittest.main()
