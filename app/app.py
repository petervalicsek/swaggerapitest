""" My swagger app's python code """
import connexion
from flask_sqlalchemy import SQLAlchemy
from flask import send_file

app = connexion.App(__name__, port=5000, host="0.0.0.0", specification_dir='swagger/')
app.app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:@db/swagger'
app.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
DB = SQLAlchemy(app.app)

class User(DB.Model):
    """ DB's swagger table's model """
    __tablename__ = 'swagger'

    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String)

    def get_id(self):
        """ returns id"""
        return self.id

    def get_name(self):
        """ returns name"""
        return self.name


DB.create_all()
DB.session.commit()

def upload_file(upfile):
    """ uploading file """
    try:
        upfile.save("/swagger/"+upfile.filename)
        ans = "Upload successful"
    except Exception as e:
        ans = ("Service Unavailable", 503)

    return ans

def download_file(filename):
    """ downloading file """
    try:
        file = send_file("/swagger/"+filename["filename"], attachment_filename=filename["filename"])
        return file
    except:
        return ("Not found", 404)

def get_last_id():
    """ returns last id from db"""
    try:
        lastid = DB.session.query(User).order_by(User.id.desc()).first()
        ans = lastid.id
    except Exception as e:
        DB.session.rollback()
        ans = ("Service Unavailable", 503)
    return ans

def get_users():
    """ returns all users data """
    try:
        userlist = DB.session.query(User).all()
        resp = []
        for user in userlist:
            resp.append({"id": user.id, "name": user.name})
    except Exception as e:
        DB.session.rollback()
        resp = ("Service Unavailable", 503)
    return resp

def get_user(id):
    """ returns one users name"""
    try:
        users = DB.session.query(User).filter(User.id == id)
        username = {"name": users[0].name}
    except:
        DB.session.rollback()
        username = ("User not found", 404)
    return username

def add_user(user):
    """ adds a new user """
    user = User(name=user['name'])
    DB.session.add(user)
    DB.session.commit()
    DB.session.refresh(user)
    return user.get_id()

def edit_user(user):
    """ edits a user """
    data = {"name": user["name"]}
    user_to_edit = DB.session.query(User).filter(User.id == user["id"]).update(data)
    DB.session.commit()
    if user_to_edit == 1:
        ans = {"id": user["id"], "name": user["name"]}
    else:
        ans = ("User not found", 404)
    return ans

def delete_user(user):
    """ deletes a user """
    user_to_delete = DB.session.query(User).filter(User.id == user['id']).delete()
    DB.session.commit()
    if user_to_delete == 1:
        ans = "Delete of id: " + str(user['id']) + " completed"
    else:
        ans = ("User not found", 404)
    return ans

if __name__ == '__main__':
    app.add_api('myswaggerapi.yaml', arguments={'title': 'Hello Swagger Example'})
    app.run(debug=True)
