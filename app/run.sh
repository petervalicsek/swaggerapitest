#!/bin/sh
mkdir test_results -p
touch pylint.txt

pylint app.py > test_results/pylint.txt

coverage run test.py
coverage html -d test_results/

python3 app.py
